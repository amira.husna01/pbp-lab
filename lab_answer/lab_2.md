Apakah perbedaan antara JSON dan XML?
- XML adalam markup language, JSON adalah data format pada aplikasi JavaScript.
- JSON memiliki ukuran file yang lebih kecil, sedangkan XML lebih cepat dalam mentransfer data.
- JSON lebih mudah dibaca, sedangkan XML lebih kompleks.
- Terkait keamanan, JSON lebih aman dari XML
Apakah perbedaan antara HTML dan XML?
- HTML digunakan untuk menampilkan data, sedangkan XML digunakan untuk mentransfer data.
- HTML lebih ke bagaimana desain web ditampilkan pada client-side.
- Pada HTML, closing tags tidak selalu dibutuhkan, sedangkan pada XML, closing tags wajib digunakan.
- HTML tidak case sensitive, sedangkan XML case sensitive




Referensi:
-https://www.imaginarycloud.com/blog/json-vs-xml/
-https://www.upgrad.com/blog/html-vs-xml/