import 'package:flutter/material.dart';

import '../dummy_data.dart';
import '../widgets/category_item.dart';

class CategoriesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(alignment: Alignment.center, children: <Widget>[
      Positioned(
          top: 20,
          child: Text(
            'Safe Flight',
            maxLines: 10,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
          )),
      Positioned(
          top: 70,
          child: Text(
            'stay happy, stay healthy',
            maxLines: 10,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 15),
          )),
      Positioned(
          top: 100,
          child: Text(
            'Selamat Datang, user!',
            maxLines: 10,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          )),
      Positioned(
          top: 150,
          child: Text(
            'Fitur yang tersedia:',
            maxLines: 10,
            textAlign: TextAlign.left,
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          )),
      Positioned(
        top: 180,
        child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Container(
              width: 150,
              height: 20,
              child: Text(
                "Info Statistik",
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyText1,
              ),
            )),
      ),
      Positioned(
        top: 210,
        child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Container(
              width: 150,
              height: 20,
              child: Text(
                "Regulasi",
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyText1,
              ),
            )),
      ),
      Positioned(
        top: 240,
        child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Container(
              width: 150,
              height: 20,
              child: Text(
                "GetSwabbed!",
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyText1,
              ),
            )),
      ),
      Positioned(
        top: 270,
        child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Container(
              width: 150,
              height: 20,
              child: Text(
                "Info Hotel Karantina",
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyText1,
              ),
            )),
      ),
      Positioned(
        top: 300,
        child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Container(
              width: 150,
              height: 20,
              child: Text(
                "Artikel",
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyText1,
              ),
            )),
      ),
      Positioned(
        top: 330,
        child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Container(
              width: 150,
              height: 20,
              child: Text(
                "Bantuan",
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyText1,
              ),
            )),
      ),
    ]);
  }
}
