import 'package:flutter/material.dart';

class MyCustomForm extends StatefulWidget {
  const MyCustomForm({Key key}) : super(key: key);

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();
  String _judul = " ";
  String _gambar = " ";
  String _deskripsi = " ";
  String _isi = " ";

  Widget _buildJudul() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Judul'),
      maxLength: 1024,
      validator: (String value) {
        if (value.isEmpty) {
          return 'Pertanyaan ini wajib diisi';
        }

        return null;
      },
      onSaved: (String value) {
        _judul = value;
      },
    );
  }

  Widget _buildGambar() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Link Gambar'),
      maxLength: 1024,
      validator: (String value) {
        if (value.isEmpty) {
          return 'Pertanyaan ini wajib diisi';
        }

        return null;
      },
      onSaved: (String value) {
        _gambar = value;
      },
    );
  }

  Widget _buildDeskripsi() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Deskripsi'),
      maxLength: 1024,
      validator: (String value) {
        if (value.isEmpty) {
          return 'Pertanyaan ini wajib diisi';
        }

        return null;
      },
      onSaved: (String value) {
        _deskripsi = value;
      },
    );
  }

  Widget _buildIsi() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Isi'),
      maxLength: 1024,
      validator: (String value) {
        if (value.isEmpty) {
          return 'Pertanyaan ini wajib diisi';
        }

        return null;
      },
      onSaved: (String value) {
        _isi = value;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Tulis Artikel")),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(24),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  _buildJudul(),
                  _buildGambar(),
                  _buildDeskripsi(),
                  _buildIsi(),
                  SizedBox(height: 100),
                  RaisedButton(
                    child: Text(
                      'Submit',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                    color: Colors.deepPurple,
                    onPressed: () {
                      if (!_formKey.currentState.validate()) {
                        return;
                      }

                      _formKey.currentState.save();

                      print(_judul);
                      print(_gambar);
                      print(_deskripsi);
                      print(_isi);
                    },
                  )
                ],
              ),
            ),
          ),
        ));
  }
}
