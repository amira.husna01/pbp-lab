from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from .models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required

@login_required(login_url='/admin/login/')
# Create your views here.
def index(request):
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

def add_friend(request):
    form = FriendForm(request.POST or None)  # TODO Implement this
    if (form.is_valid and request.method == 'POST'):
        form.save()
    return render(request, 'lab3_form.html')