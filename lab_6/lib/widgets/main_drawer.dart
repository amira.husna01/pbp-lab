import 'package:flutter/material.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon, Function tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Theme.of(context).accentColor,
            child: Text(
              'Fitur',
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 30,
                  color: Theme.of(context).primaryColor),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          buildListTile('Info Statistik', Icons.bar_chart, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Regulasi', Icons.info_outline, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('GetSwabbed!', Icons.health_and_safety_outlined, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Info Hotel Karantina', Icons.home_filled, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Artikel', Icons.article, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
        ],
      ),
    );
  }
}
