import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/screens/form_artikel.dart';

import '../models/meal.dart';

class FavoritesScreen extends StatelessWidget {
  final List<Meal> favoriteMeals;

  FavoritesScreen(this.favoriteMeals);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _artikelGenerator(context),
      floatingActionButton: _getArtikelFloatingActionButton(context),
    );
  }

  Widget _artikelGenerator(BuildContext context) {
    return Stack(alignment: Alignment.center, children: <Widget>[
      Positioned(
          top: 20,
          child: Text(
            'Artikel',
            maxLines: 10,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
          )),
      Positioned(
          top: 60,
          child: Text(
            'Dapatkan Tips',
            maxLines: 10,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 15),
          )),
      Positioned(
          top: 80,
          child: Text(
            '&',
            maxLines: 10,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 15),
          )),
      Positioned(
          top: 100,
          child: Text(
            'Bagikan Pengalaman Menyenangkan Saat Berpergian',
            maxLines: 10,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 15),
          )),
      Positioned(
        top: 130,
        // left: 50,
        child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Container(
              width: 400,
              height: 150,
              child: Text(
                "Artikel 1",
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyText1,
              ),
            )),
      ),
      Positioned(
        top: 300,
        // left: 50,
        child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Container(
              width: 400,
              height: 150,
              child: Text(
                "Artikel 2",
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyText1,
              ),
            )),
      ),
    ]);
  }

  FloatingActionButton _getArtikelFloatingActionButton(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => MyCustomForm()),
        );
      },
      child: Icon(Icons.edit),
      backgroundColor: Colors.deepPurple,
      foregroundColor: Colors.white,
    );
  }
}
