from django.shortcuts import render
from django.http.response import HttpResponse
from .models import Note
from .forms import NoteForm
from django.core import serializers

def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
    return render(request, "lab4_form.html")

def note_list(request):
    notes1 = Note.objects.all().values()
    response = {'notes1': notes1}
    return render(request, 'lab4_note_list.html', response)

def json(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")