from django.shortcuts import (get_object_or_404, render, HttpResponseRedirect)
from lab_4.models import Note
from .forms import NoteForm
from django.core import serializers
from django.http import JsonResponse
from django.http.response import HttpResponse
from django.core import serializers

# Create your views here.
def index(request):
    notes = Note.objects.all()
    form = NoteForm()

    if request.method == "POST" and form.is_valid():
        form  = NoteForm(request.POST)
        form.save()

    response = {'notes': notes}
    response['form'] = form
    return render(request, 'lab5_index.html', response)

def get_note(request, id):
    note = serializers.serialize("json",Note.objects.filter(pk=id))
    return HttpResponse(note, content_type="application/json")

def delete_note(request, id):
    obj = get_object_or_404(Note, id = id)
    if request.method == 'POST':         
        obj.delete()    
    note = serializers.serialize('json', Note.objects.all())     
    return HttpResponse(note, content_type='application/json')

def update_note(request, id):
    obj = get_object_or_404(Note, id = id)
    # form = NoteForm(request.POST or None, instance = obj)
    if request.is_ajax():
        new_from_m = request.POST.get("from_m")
        new_to = request.POST.get("to")
        new_title = request.POST.get("title")
        new_message = request.POST.get("message")
        obj.to = new_to
        obj.from_m = new_from_m
        obj.title = new_title
        obj.message = new_message
        obj.save()
    # if (form.is_valid() and request.method == 'POST'):
    #     form.save()
    # note = serializers.serialize('json', Note.objects.all())  
    # return HttpResponse(note, content_type='application/json')     
    return HttpResponse(serializers.serialize('json', [obj, ]), content_type='application/json')
    

def json(request):
    note_html = serializers.serialize("json",Note.objects.all())
    return JsonResponse(note_html, safe=False)