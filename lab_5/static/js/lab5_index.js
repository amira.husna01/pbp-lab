$(document).ready(function(){

    TableValue()

    //load data ke page
    function TableValue() {
      setTimeout(function(){
      $.ajax({
        url: "http://127.0.0.1:8000/lab-4/json", 
        dataType: 'json',
        success: function(hasil){
          for(i=0; i < hasil.length; i++){
            var tempB = "<button type='button' id='"+hasil[i].pk+"' class='view btn btn-primary'>View</button><button type='button' id='"+hasil[i].pk+"' class='show-edit-modal btn btn-success'>Edit</button><button type='button' id='"+hasil[i].pk+"' class='deleteWarn btn btn-danger'>Delete</button>"
            var temp = "<tr><td scope='row col-2'>"+hasil[i].fields.to+"</td><td class='col-2'>"+hasil[i].fields.from_m+"</td><td class='col-2'>"+hasil[i].fields.title+"</td><td>"+hasil[i].fields.message+"</td><td class='col-1'>"+tempB+"</td></tr>"
            $("#tr_note").append(temp);
          }
      }})
    },500);}

    //nampilin detail note ke modal
    $('#tr_note').on("click",'.view',function(){
      console.log("ini view")
      var id = $(this).attr('id');
        $.ajax({
        url: 'http://127.0.0.1:8000/lab-5/notes/'+id, 
        dataType: 'json',
        success: function(hasil){
          var temp1 = "<div class='modal-header'><h4 class='modal-title d-flex justify-content-center'>"+hasil[0].fields.title+"</h4><h5 class='modal-title'>To: "+hasil[0].fields.to+"</h5><br><h5 class='modal-title'>From: "+hasil[0].fields.from_m+"</h5><br><button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button></div><div class='modal-body'><p>"+hasil[0].fields.message+"</p></div><div class='modal-footer'><button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Close</button></div>";
          $('body').append("<div id='modal-view' class='modal' tabindex='-1'><div class='modal-dialog modal-dialog-centered modal-dialog-scrollable'><div class='modal-content'>"+temp1+"</div></div></div>");
          $('#modal-view').modal('show');
        }})
    });

    //untuk nampilin modal
    let pk = "";
    $('#tr_note').on("click",'.show-edit-modal',function(){
      console.log("ini edit");
      pk = $(this).attr('id');
      $('#modal-edit').modal('show');
    });

    //update, disubmit menggunakan form di html
    $("#form-update").submit(function(e) {
      e.preventDefault();

      $.ajax({
        type: 'POST',
        url: '/lab-5/notes/'+pk+'/update',
        dataType: 'json',
        data: {
          'title': $("#input-from").val(),
          'to': $("#input-to").val(),
          'from_m': $("#input-title").val(),
          'message': $("#input-message").val(),
          'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val(),
        },
        success: function (response) {
          $("#input-from").val("") 
          $("#input-to").val("") 
          $("#input-title").val("") 
          $("#input-message").val("")

          $("#tr_note").empty();
          TableValue()
        }})
    });

    //menghapus
    $('#modal-delete').on("click",'.delete',function(){
      var id = $(this).attr('id');
        $.ajax({
        url: 'http://127.0.0.1:8000/lab-5/notes/'+id+'/delete', 
        type: 'POST',
        data: {'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val()},
        dataType: "json",
        success: function(hasil){
        }})
    });

    //menampilkan modal delete
    $('#tr_note').on("click",'.deleteWarn',function(){
      var id = $(this).attr('id');
        $.ajax({
        url: 'http://127.0.0.1:8000/lab-5/notes/'+id, 
        data: {'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val()},
        dataType: 'json',
        success: function(hasil){
          $('.delete').remove();
          $('#div-input-delete').append("<input id='"+hasil[0].pk+"' class='delete btn btn-danger' type='submit' Value='Hapus'>")
          $('#modal-delete').modal('show');
        }})
    });
});