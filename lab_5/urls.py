from django.urls import path
from .views import index, get_note, json, delete_note, update_note

urlpatterns = [
    path('', index, name='index'),
    path('notes/<id>', get_note, name='note-detail'),
    path('notes/<id>/delete', delete_note),
    path('notes/<id>/update', update_note),
    path('json', json, name='json'),
]